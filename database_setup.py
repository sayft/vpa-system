import csv
from database import database_worker
from datetime import date

from vpa import helpers
import os
import argparse
import logging

logger = logging.getLogger("database_setup")
logger.setLevel(logging.INFO)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname) - %(message)s')
console_handler.setFormatter(formatter)

log_dir = os.path.join("./logs")
for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
logging.basicConfig(
            filename = os.path.join(log_dir, "database_setup_{date_today_val}.log".format(date_today_val = date.today().strftime("%d%m%Y"))),
            format = '%(asctime)s %(levelname)-1s %(message)s',
            level = logging.INFO,
            datefmt = '%d-%m-%Y %H:%M:%S')
logger = logging.getLogger(__name__)

def main():

    parser = argparse.ArgumentParser(description = "Perform a database setup or tear down.")
    parser.add_argument("--db_name", type = str, help = "Database name from db_connection object")
    parser.add_argument("--db_host", type = str, help = "Database host server value, i.e. 127.0.0.1")
    parser.add_argument("--db_socket", type = str, help = "Database socket, i.e. /tmp/mysql.sock")
    parser.add_argument("--db_option", type = str, help = "--db_option create - create a table  in a database, drop - drop a table in a database.")
    parser.add_argument("--env", type = str, help = "Environment variable name for the table creation.")
    parser.add_argument("--freq", type = str, help = "Frequency of the table, i.e. daily, weekly, monthly.")
    parser.add_argument("--table_def_file", type = str, help = "Path and table definition file name.")
    parser.add_argument("--symbols_file", type = str, help = "Path and name of symbol file.")

    args = parser.parse_args()
    db_user = os.environ['DB_USER']
    db_password = os.environ['DB_USER_PASSWORD']
    if not db_user:
        logger.error("DB_USER environment variable is not set or empty, can not proceed.")
        exit(1)
    if not db_password:
        logger.error("DB_USER_PASSWORD environment variable is not set or empty. can not proceed.")
        exit(1)
    db_connection = database_worker.set_connection_pymysql(
            database = args.db_name,
            username = db_user,
            password = db_password,
            host = args.db_host,
            socket = args.db_socket)
    
    if args.db_option == 'create':
        table_column_types = database_worker.read_table_info(args.table_def_file)
        try:
            with open(os.path.join(args.symbols_file)) as f:
                reader = csv.DictReader(f)
                for row in reader:
                    symbol = row['ASX code']
                    logger.info("Using {symbol_val} to generate asx_{table_val}_{freq_val}_{env_val}".format(
                        symbol_val = symbol,
                        table_val = symbol.lower(),
                        freq_val = args.freq,
                        env_val = args.env))
                    database_worker.create_table(
                        table = "asx_" + symbol.lower(),
                        freq = args.freq,
                        env = args.env,
                        connection = db_connection,
                        table_structure = table_column_types)
        except Exception as e:
          logger.error("Failed to create table + {error_val}".format(error_val = e))
    elif args.db_option == 'drop':
        tables = database_worker.get_all_tables(database = args.db_name, connection = db_connection)
        for db_table in tables:
            logger.info("Dropping {table_val}".format(table_val = db_table))
            database_worker.drop_table(table = db_table, connection = db_connection)

if __name__ == '__main__':
    main()
