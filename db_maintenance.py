from database import database_worker as dw
from database import database_maintenance as dm
from datetime import date
from vpa import helpers
import os
import argparse
import logging

logger = logging.getLogger("database_setup")
logger.setLevel(logging.INFO)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname) - %(message)s')
console_handler.setFormatter(formatter)

log_dir = os.path.join("./logs")
for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
logging.basicConfig(
            filename = os.path.join(log_dir, "db_maintenance_{date_today_val}.log".format(date_today_val = date.today().strftime("%d%m%Y"))),
            format = '%(asctime)s %(levelname)-1s %(message)s',
            level = logging.INFO,
            datefmt = '%d-%m-%Y %H:%M:%S')
logger = logging.getLogger(__name__)


def main():

    parser = argparse.ArgumentParser(description="Database maintenance")
    parser.add_argument("--db_name", type = str, help = "Database name from db_connection object")
    parser.add_argument("--db_host", type = str, help = "Database host server value, i.e. 127.0.0.1")
    parser.add_argument("--db_socket", type = str, help = "Database socket, i.e. /tmp/mysql.sock")
    parser.add_argument("--env", type = str, help = "Environment variable name for the table creation.")
    parser.add_argument("--freq", type = str, help = "Frequency of the table, i.e. daily, weekly, monthly.")
    parser.add_argument("--table_def_file", type = str, help = "Path and table definition file name.")
    parser.add_argument("--symbols_file", type = str, help = "Path and name of symbol file.")

    args = parser.parse_args()
    db_user = os.environ['DB_USER']
    db_password = os.environ['DB_USER_PASSWORD']

    if not db_user:
        logger.error("DB_USER environment variable is not set or empty, can not proceed.")
        exit(1)
    if not db_password:
        logger.error("DB_USER_PASSWORD environment variable is not set or empty. can not proceed.")
        exit(1)
    db_connection = dw.set_connection_pymysql(
            database = args.db_name,
            username = db_user,
            password = db_password,
            host = args.db_host,
            socket = args.db_socket)

    current_tables = dw.get_all_tables(database=args.db_name, connection=db_connection)
    asx_symbols_tables = dm.get_latest_table_names(tables_file=os.path.join(args.symbols_file), table_prefix="asx_", table_suffix="_{freq}_{env}".format(freq=args.freq, env=args.env))

    tables_to_be_removed = helpers.get_leftover_items(data1=asx_symbols_tables, data2=current_tables)
    if not tables_to_be_removed:
        logger.info("Database is up to date, no maintenance is required.")
    else:
        logger.info("Tables to be dropped: ", tables_to_be_removed)

        for db_table in tables_to_be_removed:
            logger.info("Dropping {table_val}".format(table_val = db_table))
            dw.drop_table(table = db_table, connection = db_connection)
    logger.info("Maintenace run complete.")

if __name__ == '__main__':
    main()
