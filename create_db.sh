#!/bin/bash
python3 database_setup.py --db_name $DB_NAME --db_host $DB_HOST --db_socket $DB_SOCKET --db_option create --env $DB_ENV --freq $TABLE_FREQ --table_def_file $TABLE_DEF_FILE --symbols_file $SYMBOLS_FILE
