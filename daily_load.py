import logging
import os
import time
import argparse

from datetime import date
from operator import attrgetter

from api import api_connector
from database import database_worker
from vpa import helpers, symbols_worker

def main():
    
    parser = argparse.ArgumentParser(description = "Perform a daily load of ASX symbols")
    parser.add_argument("--db_name", type = str, help = "Database name from db_connection object")
    parser.add_argument("--db_host", type = str, help = "Database host server value, i.e. 127.0.0.1")
    parser.add_argument("--db_socket", type = str, help = "Database socket, i.e. /tmp/mysql.sock")
    parser.add_argument("--table_def_file", type = str, help = "Path and table definition file name.")
    parser.add_argument("--symbols_file", type = str, help = "Path and name of symbol file.")
    parser.add_argument("--api_key_file", type = str, help = "Path and name of api key file.")
    parser.add_argument("--timezone_format", type = str, help = "Pytz compatible timezone info i.e. Australia/Sydney")
    parser.add_argument("--date_format", type = str, help = "Date format i.e. %Y-%m-%d")
    parser.add_argument("--filter_column", type = str, help = "Column name that exists in both API and SQL outputs that will be used to filter data")
    parser.add_argument("--table_suffix", type = str, help = "Parse table suffix that will be used for grabbing data for a symbol from table, i.e. _daily_staging")
    
    args = parser.parse_args()

    log_dir = os.path.join("./logs")
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(
            filename = os.path.join(log_dir, "daily_load{date_today_val}.log".format(date_today_val = date.today().strftime("%d%m%Y"))),
            format = '%(asctime)s %(levelname)-1s %(message)s',
            level = logging.INFO,
            datefmt = '%d-%m-%Y %H:%M:%S')
    logger = logging.getLogger(__name__)
    input_dir = os.path.join("./input")
    asx_companies = helpers.get_symbols_meta(os.path.join(args.symbols_file))
    api_key = helpers.read_file(args.api_key_file)
    db_user = os.environ['DB_USER']
    db_password = os.environ['DB_USER_PASSWORD']
    if not db_user:
        logger.error("DB_USER environment variable is not set or empty, can not proceed.")
        exit(1)
    if not db_password:
        logger.error("DB_USER_PASSWORD environment variable is not set or empty. can not proceed.")
        exit(1)
    db_con = database_worker.set_connection_pymysql(
            database = args.db_name,
            username = db_user,
            password = db_password,
            host = args.db_host,
            socket = args.db_socket)
    table_struct = database_worker.read_table_info(args.table_def_file)
    date_today = date.today().strftime("%d-%m-%Y")
    logging.info("Beginning data load/updates  for {date_val} AEST".format(date_val = date_today))

    for company in asx_companies:
        api_url = api_connector.get_eod_url(
                symbol = company.code,
                api_key = api_key)
        logging.info("Attempting to download data from API call: {api_url_val}".format(
            api_url_val = api_url))
        api_data = api_connector.get_data_from_api(url = api_url)
        api_error_check = api_connector.api_errors(output = api_data)

        if api_connector.error_check_failed(error_check = api_error_check, symbol_code = company.code) is True:
            continue

        workable_eod_data = api_connector.get_workable_eod_data(eod_data = api_data, items_to_exclude = {'', "", 0, '0', "0"})
        api_error_check = api_connector.api_errors(output = workable_eod_data)

        if api_connector.error_check_failed(error_check = api_error_check, symbol_code = company.code) is True:
            continue

        symbol_table = "asx_{symbol_code_val}{table_suffix_val}".format(
                symbol_code_val = company.code.lower(),
                table_suffix_val = args.table_suffix)
        company_sql_result = database_worker.select_from_table(
                table = symbol_table,
                connection = db_con)
        company_sql_data = helpers.get_symbol_data_from_sql(sql_result = company_sql_result)
        symbol_data = helpers.get_symbol_data_from_csv(csv_data = workable_eod_data)
        symbol_data_with_meta = helpers.set_symbols_meta(
                symbol_meta = company,
                symbol = symbol_data)
        # set appropraite unixtimestamp value based on Austrlian date from the API result
        logging.info("Applying transformations on {company_code_val}".format(company_code_val = company.code))
        transformed_symbol = symbols_worker.apply_transformations(
                symbol = symbol_data_with_meta,
                timezone_format = 'Australia/Sydney',
                date_format = '%Y-%m-%d')
        table_manipulation_decision = helpers.table_manipulation_decision(
                symbol = transformed_symbol,
                sql_result = company_sql_data,
                column_to_compare = 'date_utc_unixtimestamp')
        logging.info("Determining if an UPDATE, WRITE OR SKIP is required for {table_val}".format(table_val = symbol_table))

        if table_manipulation_decision == 'Update':
            update_unixtimestamp = helpers.get_largest_value(
                    symbol = transformed_symbol,
                    column_to_use = 'date_utc_unixtimestamp')
            logging.info("An UPDATE is required. Proceeding to UPDATE {table_val}".format(table_val = symbol_table))
            database_worker.update_table(
                    table = symbol_table,
                    connection = db_con,
                    symbol = [update_row for update_row in transformed_symbol 
                        if update_row.date_utc_unixtimestamp == update_unixtimestamp][0])
        elif table_manipulation_decision == 'Skip':
            logging.info("No new data to WRITE or UPDATE for {table_val}, skipping this table".format(table_val = symbol_table))
            logging.info("-------------------------------------------------------")
            continue
        elif table_manipulation_decision == 'Write':
            logging.info("A WRITE is required. Determining what data to write to {table_val}".format(table_val = symbol_table))
            common_values_in_symbol_and_sql = helpers.get_common_records(
                    symbol = transformed_symbol,
                    query = company_sql_data,
                    column_to_compare = args.filter_column)
            symbol_data_to_write = helpers.get_filtered_data_based_on_common_values(
                   symbol = transformed_symbol,
                   common_values = common_values_in_symbol_and_sql,
                   column_to_filter = args.filter_column)
            database_worker.write_to_table(
                   table = symbol_table,
                   connection = db_con,
                   data = symbol_data_to_write,
                   table_structure = table_struct)
        logging.info("-------------------------------------------------------")
    logging.info("Completed data load/updates for {date_val} AEST".format(date_val = date.today().strftime("%d-%m-%Y")))

if __name__ == '__main__':
    main()
