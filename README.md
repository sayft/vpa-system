# Volume Price Analysis System

## Introduction

My first look into trading and stock market was with ["A Complete Guide to Volume Price Analysis"](https://g.co/kgs/CGiEQj) by Anna Coulling ISBN: 9781491249390,
I was fascinated with the approach to trading that was described in the book and for a long while I wanted to create an ASX scanner which will
scan the market, apply VPA (Volume Price Analysis) rules and tell me which stock to focus on as I do not have the time to go over 2000+ ASX 
listed companies every day.

This system aims to do just that, this is rather ambitious as translating VPA logic programatically is going to be a challenge. Early versions
of this system will include foundations for VPA, such as database table generation, ASX API scanning, data ETL and finally some very basic "rules" that will be applied on the ASX instruments.

Engineering design choices for this system may not be ideal, I am using this project to explore various database maintenance, data ingestion and ETL techniques, my engineering goal for this project is to create an autonomous VPA system with as little interaction from human element as possible.

Furthermore, early version of this codebase will deal with `Daily` data only, `Weekly` and `Monthly` data will be added as part of `Version 1` release.  

Finally, as of the time of writing this codebase was developed on MacOs using `Python 3.7.0`, if you are on Windows and wish to run the code, good luck. (OS agnostic builds are in TODO).  

# Setup

1. Install `Python 3.0.7`- [Python.org](https://www.python.org/downloads/release/python-370/)  
1. Install `MySql 8.0.12`- [MySql.com](https://downloads.mysql.com/archives/community/)  
1. Download the latest [ASX listed companies](https://www.asx.com.au/asx/research/ASXListedCompanies.csv), or use `./input/asx_symbols.csv` file provided  
1. Configure `prod` and `unittest`MySql databases (or whatever databases you wish to use, keep in mind you will need to update run scripts with new values)   
1. From source directory in your terminal, Run `pip install -r requirements.txt`  
1. Obtain an API key from [EOD historical Data](https://eodhistoricaldata.com), insert this key into `./api/input/eod_key` file  
1. Export `DB_USER`, `DB_USER_PASSWORD`, `DB_USER_UNITTEST` and `DB_USER_UNITTEST_PASSWORD` environment variables  
1. Execute `database_setup` using the following:
`python3 database_setup.py --db_name ENV_NAME_FOR_DB  --db_host DB_HOST --db_socket DB_SOCKET --db_option create --env TABLE_ENV --freq TABLE_FREQ --table_def_file TABLE_DEFINITIONS_FILE --symbols_file ASX_SYMBOLS_FILE`  

Where:  
`--db_name` database name to connect to i.e. `prod` or `staging` database  
`--db_host` database hostname i.e. 127.0.0.1  
`--db_socket` pymysql database socket path i.e. `/tmp/mysql.sock`  
`--db_option` either `create` or `drop`, `create` will use `--table_def_file` and `--symbols_file` to generate database tables, `drop` option will drop all current tables in the database  
`--env` environment suffix for the table, i.e asx_abc_`prod`_daily  
`--freq` frequency suffix for the table , i.e. asx_abcprod_`daily`  
`--table_def_file` relative path and file name of a table structure file i.e. `./database/input/table_info.txt`. Uses the following format:  
    ```
    column_name datatype,
    column_name datatype,
    PRIMARY KEY(column_name)
    ```

`--symbols_file` relative path and file name of ASX listed companies file i.e. `./input/asx_symbols.csv`

# Data load run

1. Optional: Set-up a cron job or your choice of scheduler to execute `daily_load.sh`  
1. a) From source directory in command like execute `./daily_load.sh` (ensure the arguments are filled out there)  
b) Alternatively from source directory execute:  
`python3 daily_load.py --db_name DB_NAME --db_host DB_HOST --db_socket DB_SOCKET --table_def_file TABLE_DEFINITION_FILE  --symbols_file ASX_SYMBOLS_FILE --api_key_file API_KEY_FILE --timezone_format TIMEZONE_FORMAT --date_format DATE_FORMAT --filter_column FILTER_COLUMN --table_suffix TABLE_SUFFIX`  

Where:  
`--db_name` database name to connect to i.e. `prod` or `staging` database  
`--db_host` database hostname i.e. 127.0.0.1  
`--db_socket` pymysql database socket path i.e. `/tmp/mysql.sock`   
`--table_def_file` relative path and file name of a table structure file i.e. `./database/input`  
`--symbols_file` relative path and file name of ASX listed companies file i.e. `./input/asx_symbols.csv`  
`--api_key` Relative path and name of an API key file, the contents of that file must be an API key  
`--timezone_format` A `pytz` compatible timezone format used for unix timestamp conversion, by default should be `Australia/Sydney`  
`--date_format` A `pytz` compatible date format used to process API date values  
`--filter_column` column name to use for `Symbol` namedtuple filtering i.e. `date_utc_unixtimestamp`  
`--table_suffix` table suffix to be able to read particular databse i.e. asx_moq_daily_staging would have table suffix of  `_daily_staging`

# VPA rule run

From source run the following
`python3 get_rules_out.py --db_name DB_NAME --db_host DB_HOST --db_socket DB_SOCKET --symbols_file SYMBOLS_FILE --table_suffix TABLE_SUFFIX --max_identifier MAX_IDENTIFIER --output_dir DIRECTORY`  

Where the parameters have the same values as previous except for the following new parameters:  

`--table_suffix` table suffix that will be used to build table name for VPA rule generation must be in format `_frequency_env` i.e. `_daily_staging`  
`--max_identifier` column name to use to determine maximum `Symbol` namedtuple from a list of namedtuples i.e. `date_utc_unixtimestamp  
`--output_dir` directory where `vpa_rules.txt` file will be written to i.e `/var/www/html/` or `./output`  


# Unit tests

1. Ensure that `DB_USER_UNITTEST` and `DB_USER_UNITTEST_PASSWORD` environment variables are set  
1. Ensure that `host` and `socket` values are set appropritely in `./database/test/test_database_worrker.py`  
1. From source directory in command line run `./run_tests.sh` to execute unit tests, you will need to ensure that `DB_USER_UNITTEST` is able to `READ`, `WRITE` and `DELETE` from `unittest` database  
# Logs

Logs are kept in `./logs` directory.

# Logic flow

Below image outline both `database_setup` and `daily_load` logic flow. I did not include logic flow for `get_rules_out` as the rules will change.  

![logic_flow](images/Database_setup_and_data_load_flow.png)  


# Version History

0.8.2 - Add database maintenance logic to delete tables based on current tables and table file  
0.8.1 - Add typed hints and refactor  
0.8 - Add dynamic rule check functionality  
0.7.2.3 - Add monotonic increase/decrease over X consecutive periods rule  
0.7.2.2 - Add logic flow images to README  
0.7.2.1 - Add LowAndVolumeLowerThanPrev rule to make basic rule output a little more useful  
0.7.2 - Read databse user/password from environment, change log file output to include date is ddmmYYYY format, add `get_rules_out.sh` wrapper  
0.7.1.1 - Parametrise table name in `daily_load.py`  
0.7.1 - Parametrise output directory for `get_rules_out.py`  
0.7 - Add `ge_rules_out.py` to process and output VPA rules results. Added defensive timeout for `requests.get()`  
0.6 - Rework `UPDATE`,` WRITE` and `SKIP` logic for table writing, added a defensive cast try when reading API data  
0.5.3.1 - Update `volume` and `prev_volume` datatypes to be `bigint` instead of `decimal`, updated default value in `Symbol` namedtuple for `volume` and `prev_volume` fields, also updated unittests to use new datatypes for these two columns  
0.5.3 - Added logic to remove 0 value trading data, reworked API error checking logic  
0.5.2 - Rework Update table logic, now updates wont be more important than a write operation. Improved logging messages   
0.5.1 - Refactor codebase to use parametrised common values filter for API and SQL output  
0.5 - Add Update table logic to deal with Intraday loads, improve logging messaging, changed API choice, added API error checking and cleanup, added log file by date generation, improved `Symbol` namedtuple, added utctimestamp logic, added unit tests for various new methods  
0.4 - Add basic VPA rules logic and include `prev_close` to `Symbol` namedtuple  
0.3 - Add basic AlphaVantage API connector  
0.2.1 - Refactor database code to be able to write to table using column names dynamically and ensure namedtuples are handled by `database_worker`  
0.2 - Add basic Symbol data manipulation, get/set prev value, use namedtuples  
0.1 - Add basic database functionality 
