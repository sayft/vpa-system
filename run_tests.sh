#!/bin/sh

echo "\nRunning mypy checks"
echo "\ntest_helpers"
mypy --no-warn-no-return vpa/test/test_helpers.py
echo "\ntest_symbols_worker"
mypy --no-warn-no-return vpa/test/test_symbols_worker.py
echo "\ntest_vpa_rules"
mypy --no-warn-no-return vpa/test/test_vpa_rules.py
echo "\ntest_vpa_worker"
mypy --no-warn-no-return vpa/test/test_vpa_worker.py
echo "\ntest_api"
mypy --no-warn-no-return api/test/test_api.py
echo "\ntest_database_worker"
mypy --no-warn-no-return database/test/test_database_worker.py

echo "\ntest_helpers"
python3 -m unittest vpa.test.test_helpers
echo "\ntest_symbols_worker"
python3 -m unittest vpa.test.test_symbols_worker
echo "\ntest_database_worker"
python3 -m unittest database.test.test_database_worker
echo "\ntest_database_maintenance"
python3 -m unittest database.test.test_database_maintenance
echo "\ntest_vpa_rules"
python3 -m unittest vpa.test.test_vpa_rules
echo "\ntest_vpa_worker"
python3 -m unittest vpa.test.test_vpa_worker
echo "\ntest_api"
python3 -m unittest api.test.test_api
