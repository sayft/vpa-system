#!/bin/bash
python3 daily_load.py --db_name $DB_NAME --db_host $DB_HOST --db_socket $DB_SOCKET --table_def_file $TABLE_DEF_FILE  --symbols_file $SYMBOLS_FILE --api_key_file $API_KEY_FILE --timezone_format $TIMEZONE_FORMAT --date_format $DATE_FORMAT --filter_column $FILTER_COLUMN --table_suffix $TABLE_SUFFIX
