* Add vpa rule output to a web page  
* Add symbol candle stick classificaiton  
* Add more rules (hammer candle, grave candle, etc...)  
* Improve website look by adding dynamic candlestick graphs  
* Parametrise API use  
* Add database cleaning and maintenance scripts  
* Add a VPA rule validator for custom user defined rules  
