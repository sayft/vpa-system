import logging
import mypy

from time import time
from operator import attrgetter
from vpa import helpers
from typing import List

logging.basicConfig(
        format = '%(asctime)s %(levelname)-1s %(message)s',
        level = logging.WARN,
        datefmt = '%d-%m-%Y %H:%M:%S')

def get_prev_value(symbol, field_to_work_on) -> List:
    """
    Look into sorted (descending by date) symbol data named tuple and get previous value from a particular field.

    Return a list of previous values.

    args:
      @symbol - A descending sorted named tuple representing stock symbol data
      @field_to_work_on - Represents a particular field from symbol argument to use to grab previous value, i.e. symbol[0].volume
    """

    prev_values = [] # type: List

    for row in range(0, len(symbol)):
        if row == len(symbol) - 1:
            prev_values.append(0)
        else:
            prev_values.append(symbol[row + 1].__getattribute__(field_to_work_on))

    return prev_values

def set_prev_val(symbol, prev_values, field_to_update) -> List:
    """
    Iterate over previous values and create a new copy of symbols named tuple.
    
    Return a copy of sumbol named tuple with replacaed previous values of some field_to_update

    args:
      @symbol - A descending sorted named tuple representing stock symbol data
      @prev_values - A list of previous values (from a descending sorted Symbol)
      @field_to_update - A field in a Symbol named tuple to update
   """
    symbol_copy = symbol

    for row in range(0, len(symbol_copy)):
        symbol_copy[row] = symbol[row]._replace(**{field_to_update: prev_values[row]})

    return symbol_copy

def get_data_to_write_to_table(symbol_data, query_data, filter_to_use) -> List:
    """
    Given some list of Symbol data and a SQL query result, obtain common values between these two lists and return
    a list that contains Symbol data with common values removed.

    args:
      @symbol_data - A list that contains Symbol namedtuple
      @query_data - A list of symbol data that is returned from a SQL result
      @filter_to_use = A column name to use to filter API and SQL results

    return - list of Symbol namedtuples which are filtered against common values present in both arguments
    """

    dates = helpers.get_common_records(
            symbol = symbol_data, 
            query = query_data,
            column_to_compare = filter_to_use)

    symbol_data_filtered = helpers.get_filtered_data_based_on_common_values(
            symbol = symbol_data, 
            common_values = dates,
            column_to_filter = filter_to_use)

    return symbol_data_filtered

def apply_transformations(symbol, timezone_format, date_format) -> List:
    """
    Given a Symbol namedtiple list, create a copy of the list and then perform a series of transformations on this copy, finally, return
    the copy list ready to be written to a database

    args;
      @symbol - Symbol namedtuple list to work on
      @timezone_format - A pytz compatbile timezone format string, i.e. 'Australia/Sydney'
      @date_format - A datetime compatible date format string i.e. '%Y-%m-%d'

    """

    symbol_copy = symbol

    update_timestamp = helpers.set_unixtimestamp_from_aus_date(
            symbol = symbol_copy,
            timezone_format = timezone_format,
            date_format = date_format)
    sorted_by_unixtime = sorted(update_timestamp, key = attrgetter('date_utc_unixtimestamp'), reverse = True)

    # get a bunch of previous values
    prev_open = get_prev_value(symbol = sorted_by_unixtime, field_to_work_on = 'open')
    prev_high = get_prev_value(symbol = sorted_by_unixtime, field_to_work_on = 'high')
    prev_low = get_prev_value(symbol = sorted_by_unixtime, field_to_work_on = 'low')
    prev_close = get_prev_value(symbol = sorted_by_unixtime, field_to_work_on = 'close')
    prev_volume = get_prev_value(symbol = sorted_by_unixtime, field_to_work_on = 'volume')

    # set a bunch of previous values
    with_prev_open = set_prev_val(
            symbol = sorted_by_unixtime,
            prev_values = prev_open,
            field_to_update = 'prev_open')
    with_prev_high = set_prev_val(
            symbol = with_prev_open,
            prev_values = prev_high,
            field_to_update = 'prev_high')
    with_prev_low = set_prev_val(
            symbol = with_prev_high,
            prev_values = prev_low,
            field_to_update = 'prev_low')
    with_prev_close = set_prev_val(
            symbol = with_prev_low,
            prev_values = prev_close,
            field_to_update = 'prev_close')
    with_prev_volume = set_prev_val(
            symbol = with_prev_close,
            prev_values = prev_volume,
            field_to_update = 'prev_volume')
    with_weekdays = helpers.update_day_of_the_week(
            symbol = with_prev_volume,
            date_format = date_format)

    #grab current unixtime, this will be used to update the update_unixtime field
    unixtime_now = int(time())
    set_update_unixtime = helpers.update_unixtimestamp(
            symbol = with_weekdays,
            unixtimestamp = unixtime_now)

    return set_update_unixtime
