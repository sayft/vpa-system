import logging
import mypy

from operator import attrgetter
from vpa import  vpa_rules

logging.basicConfig(
        format = '%(asctime)s %(levelname)-1s %(message)s',
        level = logging.WARN,
        datefmt = '%d-%m-%Y %H:%M:%S')

def latest_attr_less_than_prev(symbol, comparison_attribute, comparison_prev_attribute) -> bool:
    """
    Given first item in a list of Symbol namedtuples, determine if latest chosen attribute is lower than previous value 
def last_close_less_than_prev(symbol):
    """
    if symbol.close < symbol.prev_close:
        return True
    else:
        logging.warning("{symbol_val} does not qualify, previous close: {prev_close_val} is greater than today's close: {close_val}".format(
            symbol_val = symbol.code,
            prev_close_val = symbol.prev_close,
            close_val = symbol.close))
        return False

def last_low_less_than_prev(symbol) -> bool:
    """
    Given a Symbol namedtuple, determine if today's low is less than previous low. If true return true else warning.

    args:
      @symbol - A Symbol namedtuple
    """

    if symbol.low < symbol.prev_low:
        return True
    else:
        logging.warning("{symbol_val} does not qualify, previous low: {prev_low_val} is greater than today's low: {low_val}".format(
            symbol_val = symbol.code,
            prev_low_val = symbol.prev_low,
            low_val = symbol.low))
        return False

def last_low_and_volume_less_than_prev(symbol) -> bool:
    """
    Given a Symbol namedtuple, determine if today's low and today's volume is lass than previous day's low and volume, if not return a warning.

    args:
      @symbol - A Symbol namedtuple
    """

    if symbol.low < symbol.prev_low and symbol.volume < symbol.prev_volume:
        return True
    else:
        logging.warning("{symbol_val} does not qualify, either current low: {low_val} or current volume: {vol_val} (or both) are not lower than previous low: {prev_low_val} or previous volume: {prev_vol_val}".format(
            symbol_val = symbol,
            low_val = symbol.low,
            vol_val = symbol.volume,
            prev_low_val = symbol.prev_low,
            prev_vol_val = symbol.prev_volume))
        return False

def last_n_monotonic_operation(symbol, num_intervals, operation) -> bool:
    """
    Given a list of descending by date list of symbol data, perform a check if a value has been monotonically decreasing or incresing for some interval
    
    args:
      @symbol - A single list of required values to check from a list of Symbol named tuples. I.e. volume
      @num_intervals - Number of intervals to start the check for monotonic decrease from
      @operation - either 'decreasing' or 'increasing' operation to perform
    """
    
    if len(symbol) < num_intervals:
       logging.warning("Number of elements in list of Symbols: {num_elements_val}, this is less than chosen number of intervals: {num_intervals_val}".format(num_elements_val = len(symbol), num_intervals_val = num_intervals))

    #get only required length for Symbol list
    filtered_symbol = symbol[0:num_intervals]

    if operation.lower() == 'decreasing':
        return all(x <= y for x, y in zip(filtered_symbol, filtered_symbol[1:]))
    elif operation.lower() == 'increasing':
        return all(x>=y for x, y in zip(filtered_symbol, filtered_symbol[1:]))
    else:
        logging.error("Accepted operaton vaulues are 'increasing' or 'decreasing' only. Found operation value: {operation_val} is not supported".format(operation_val = operation))
        return False

