import csv
import datetime
import operator
import os
import requests
import datetime
import pytz
import logging
import typing
import mypy

from typing import NamedTuple, Tuple, List
from operator import attrgetter

logging.basicConfig(
        format = '%(asctime)s %(levelname)-1s %(message)s',
        level = logging.WARN,
        datefmt = '%d-%m-%Y %H:%M:%S')

class Symbol(typing.NamedTuple):
    name: str = 'DEFAULT'
    code: str = 'DEF'
    industry: str = 'Default Industry'
    date_au: str = '01/01/1970'
    open: float = 1.0
    high: float = 1.0
    low: float = 1.0
    close: float = 1.0
    volume: int = 1
    weekday_au: str = 'Mon'
    date_utc_unixtimestamp: int = 0
    prev_open: float = 1.0
    prev_high: float = 1.0
    prev_low: float = 1.0
    prev_close: float = 1.0
    prev_volume: int = 1
    update_unixtimestamp: int = 1

Symbol_meta = NamedTuple('Symbol_meta', [('name', str), ('code', str), ('industry', str)])

def read_file(full_file_path) -> str:
    """
    Read a file given full file path.

    args:
      @full_file_path - full file path including filename
    """
    with open(os.path.join(full_file_path)) as f:
       file_contents = f.read()
    f.close()

    return file_contents

def get_symbols_meta(full_file_path) -> List:
    """
    Read a csv file into a Symbols_meta named tuple.

    args:
      @full_file_path - full file path including filename
    """
    symbols_data = []
    with open(os.path.join(full_file_path)) as f:
        reader = csv.reader(f, delimiter=",")
        #skip heaader
        next(reader)
        for row in reader:
            symbols_data.append(Symbol_meta(
                name = row[0],
                code = row[1],
                industry = row[2]))

    return symbols_data

def set_symbols_meta(symbol_meta, symbol) -> List:
    """
    Use symbol_meta_data to populate information for a particular Symbol named tuple.

    args:
      @symbol_meta = A Symbol_meta named tuple
      @symbol = A Symbol named tuple

    return:
      A copy of symbol containing replaced name, code and industry values for a Symbol.
    """
    symbol_copy = list(symbol)
    
    for row in range(0, len(symbol_copy)):
        symbol_copy[row] = symbol[row]._replace(
                name = symbol_meta.name,
                code = symbol_meta.code,
                industry = symbol_meta.industry)
   
    return symbol_copy

def get_number_of_elements(data, num_cols_to_omit = 0) -> int:
    """
    Read in some data, convert to list and get number of columns. Optionally omit some columns from behind.
   
    args:
      @data - some string data
      @num_cols_to_omit - number of elements to exclude from count, default 0
    """

    number_of_elements = len(data.split(",")) - num_cols_to_omit

    return number_of_elements

def get_column_names(data, num_cols_to_omit) -> List[str]:
    """
    Read a list of SQL table structure columns file. Split by new line character, optionally omit some values (usually trailing new line and PRIMARE_KEY name), Take the split and get only the first word of each element (the column name).
    
    args:
      @data - A string representation of database table column structure
      @num_cols_to_omit - number of columns to exclude. Default 0

    return:
      a list of table column names
    """

    database_table_columns = data.split("\n")
    database_table_columns_trimmed = database_table_columns[0:len(database_table_columns) - num_cols_to_omit]

    database_table_columns_names = []
    
    for column in database_table_columns_trimmed:
        database_table_columns_names.append(column.partition(' ')[0])

    return database_table_columns_names

def remove_symbol_headers(csv_data) -> List[str]:
    """
    Remove header data from csv API call, if there are any.

    args:
      @csv_data - csv ata from API call

    return - list of symbol data
    """
    
    if any(set(['timestamp', 'open', 'high', 'low', 'close', 'volume', 'Open', 'High', 'Close', 'Volume']).intersection(csv_data[0])):
        logging.info("Removing detected symbol headers in first row")
        return csv_data[1:len(csv_data)]
    else:
        return csv_data

def get_symbol_data_from_csv(csv_data) -> List:
    """
    Map a list of csv data from an API call to a list of Symbols.

    args:
      @csv_data - csv data from API call

    return: list of Symbol named tuples
    """
    
    raw_symbol_data = remove_symbol_headers(csv_data)
    symbol_tuple = []
    for row in raw_symbol_data:
        try:
            symbol_tuple.append(Symbol(
                date_au = row[0],
                open = float(row[1]),
                high = float(row[2]),
                low = float(row[3]),
                close = float(row[4]),
                volume = int(row[5]))) # careful, int cast may be different for large values in 32bit systems
        except ValueError as e:
            logging.error("An error occured during cast attempts from an API return to Symbol namedtuple for data: {row_val}\n{e_val}".format(row_val = row, e_val = e))

    return symbol_tuple

def get_symbol_data_from_sql(sql_result) -> List:
    """
    Convert a sql result into Symbol namedtuple

    args:
      @sql_result - a list of lists of data returned by a sql result
    """

    output = [] # type: List
    if not sql_result:
        logging.warning("No data found in SQL result")
        return output
    else:
        for row in sql_result:
            output.append(Symbol(
            name = row[0],
            code = row[1],
            industry = row[2],
            date_au = row[3],
            open = row[4],
            high = row[5],
            low = row[6],
            close = row[7],
            volume = row[8],
            weekday_au = row[9],
            date_utc_unixtimestamp = row[10],
            prev_open = row[11],
            prev_high = row[12],
            prev_low = row[13],
            prev_close = row[14],
            prev_volume = row[15],
            update_unixtimestamp = row[16]))

    return output

def get_common_records(symbol, query, column_to_compare) -> List:
    """
    Find and return common values based on some columns given a list of Symbol API call namedtuple and a SQL result namedtuple 

    args:
      @symbol - Symbol named tuple list
      @query - List of table results
      @column_to_compare - some column value to use to compare API result and SQL result

    return - a list of common dates
    """
    try:
        symbol_values = [row.__getattribute__(column_to_compare) for row in symbol]
    except AttributeError as error:
        logging.error("Could not find {column_to_compare_val} field in API data Symbol namedtuple".format(
            column_to_compare_val = column_to_compare))
    if not query:
        common_values = [] # type: List
        return common_values
    else:
        try:
            query_values = [row.__getattribute__(column_to_compare) for row in query]
        except AttributeError as error:
            logging.error("Could not find {column_to_compare_val} field in SQL return Symbol namedtuple".format(
                column_to_compare_val = column_to_compare))
    
    common_values = list(set(symbol_values).intersection(query_values))

    return common_values

def get_filtered_data_based_on_common_values(symbol, common_values, column_to_filter) -> List:
    """
    Use common_values list to filter out Symbol namedtuple data in order to write to database.

    args:
      @symbol - Symbol namedtuple list
      @common_values - A list of common values that were found from a SQL result mapped against Symbol namedtuple
      @column_to_filter - A column in API call to use to filter out data, this column should have same value types as return call of get_common_records() method 

    return - A list of Symbol namedtuples that do not have any records in common with data currently in SQL table based on some common value lookup
    """
    filtered_data = [row for row in symbol if row.__getattribute__(column_to_filter) not in common_values]

    return filtered_data

def get_unixtimestamp_from_aus_date(date, timezone, format) -> int:
    """
    Given Australian timezone date of format YYYY-mm-dd convert it to an equivalent Unixtimestamp accounting for any DST

    args:
      @date - Some date of some format
      @timezone - a pytz compatible timezone i.e. Australia/Sydney
      @format -  date format i.e. %Y-%m-%d
    """
    aus = pytz.timezone(timezone)
    local_datetime = aus.localize(datetime.datetime.strptime(date, format))

    return int(local_datetime.timestamp())

def set_unixtimestamp_from_aus_date(symbol, timezone_format, date_format) -> List:
    """
    Given a Symbol namedtuple list, look at the Australian date value for each record and convert it to appropriate Unixtime representation.
    Return a copy of the original symbol namedtuple with date_utc_unixtimestamp field updated.

    args:
      @symbol - Symbol namedtuple list
      @timezone_format - a pytz compatible timezone i.e. Australia/Sydney
      @date_format - some date format i.e. %Y-%m-%d
    """

    symbol_copy = list(symbol)

    for row in range(0, len(symbol_copy)):
        unixtimestamp = get_unixtimestamp_from_aus_date(
                date = symbol_copy[row].date_au,
                timezone = timezone_format,
                format = date_format)
        symbol_copy[row] = symbol[row]._replace(date_utc_unixtimestamp = unixtimestamp)
    
    return symbol_copy

def update_unixtimestamp(symbol, unixtimestamp) -> List:
    """
    Given some unixtime, take a symbol namedtuple copy and replace the default value of update_unixtimestamp field with unixtime provided

    args:
      @symbol - Symbol namedtuple list
      @unixtimestamp - some int unixtimestamp
    """

    symbol_copy = list(symbol)

    for row in range(0, len(symbol_copy)):
        symbol_copy[row] = symbol[row]._replace(update_unixtimestamp = unixtimestamp)

    return symbol_copy

def update_day_of_the_week(symbol, date_format) -> List:
    """
    Given some symbol namedtuple and date format, get a copy of the symbol and update the weekday_au value from the date_au value. Return the copy"

    args:
      @symbol - Symbol namedtuple list
      @date_format - Date format i.e. %Y-%m-%d
    """

    symbol_copy = list(symbol)

    for row in range(0, len(symbol_copy)):
        symbol_copy[row] = symbol[row]._replace(weekday_au = datetime.datetime.strptime(symbol[row].date_au, date_format).strftime("%A"))

    return symbol_copy

def get_largest_value(symbol, column_to_use):
    """
    Given a list of Symbol namedtuple and some column/variable, get the largest value present in the list

    args:
      @symbol - A Symbol namedtuple list
      @column_to_use - Column/Variable to use to figure out the max value present
    """
    try:
        max_value = max([row.__getattribute__(column_to_use) for row in symbol])

        return max_value
    except:
        logging.error("No maximum value to find for {column_val} in Symbol list: {symbol_val}".format(
            column_val = column_to_use, symbol_val = symbol))

def table_manipulation_decision(symbol, sql_result, column_to_compare):
    """
    Given an API call and a SQL result, compare the latest data for both based on some comparison column and determine what table manipulation should occur

    args:
      @symbol - Symbol namedtuple API call result
      @sql_result - Symbol namedtuple SQL result
      @column_to_compare - Column that exits in both API and SQL data that will be used as max value getter comparison between both these data sets
    """
    if not symbol:
        logging.error("Symbol API data is empty, this is catasrophic failure, no API data should have been picked up earlier in the logic")
        exit(1)
    elif not sql_result:
        logging.info("During comparison of {symbol_company_code_val} it was determined that the SQL table is empty and in turn not up to date".format(
            symbol_company_code_val = symbol[0].code))
        return 'Write'
    else:
        symbol_latest = max(symbol, key = attrgetter(column_to_compare)) 
        sql_latest = max(sql_result, key = attrgetter(column_to_compare))

        if (symbol_latest.date_utc_unixtimestamp == sql_latest.date_utc_unixtimestamp and
            symbol_latest.open == sql_latest.open and
            symbol_latest.high == sql_latest.high and 
            symbol_latest.low == sql_latest.low and
            symbol_latest.close == sql_latest.close and
            symbol_latest.volume == sql_latest.volume):

            logging.info("Latest API data for {symbol_company_code_val} does not contain any new base API data that is already in SQL table".format(
                symbol_company_code_val = symbol[0].code))
            return 'Skip'
        elif ((symbol_latest.date_utc_unixtimestamp == sql_latest.date_utc_unixtimestamp) and (
              symbol_latest.open != sql_latest.open or
              symbol_latest.high != sql_latest.high or
              symbol_latest.low != sql_latest.low or
              symbol_latest.close != sql_latest.close or
              symbol_latest.volume != sql_latest.volume)):
            logging.info("Latest API data for {symbol_company_code_val} contains newer base API data than what is already in SQL table".format(
                symbol_company_code_val = symbol[0].code))
            return 'Update'
        elif symbol_latest.date_utc_unixtimestamp > sql_latest.date_utc_unixtimestamp:
            logging.info("Latest API data for {symbol_company_code_val} does not exist in SQL output".format(
                symbol_company_code_val = symbol[0].code))
            return 'Write'

def get_leftover_items(data1: List, data2: List) -> List: 
    """
    Given 2 lists data1 and data2, get the difference between data2 and data1
    """

    leftover_items = list(set(data2) - set(data1))

    return leftover_items
