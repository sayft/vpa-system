import logging
import mypy

from vpa import helpers
from collections import namedtuple
from typing import Tuple

Rules = namedtuple("Rules", 
    'CloseLowerThanPrev SidewaysPriceForLast3Intervals LowLowerThanPrev MonotonicDecreaseLowPriceLowVolOverNPeriods MonotonicDecreaseLowPriceIncreaseVoluOverNPeriods', defaults = ([], [], [], [], []))
Industry = namedtuple("Industry", 'Industry SymbolCodes')

def add_symbol_to_rule(symbol, rules, rule) -> Tuple:
    """
    Add a particular Symbol namedtuple to a particular rule.

    args:
      @symbol - a Symbol namedtuple
      @rules - a Rules namedtuple that holds all the rules
      @rule - particular rule a Symbol named tuple will be appended to
    """

    return rules.__getattribute__(rule).append(symbol)

