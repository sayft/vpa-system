import unittest
import os
import logging
import mypy

from vpa import helpers, symbols_worker, vpa_rules

class TestVpaRules(unittest.TestCase):

    def test_add_symbol_to_rule(self) -> None:
        symbol_data = helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123)
        rules = vpa_rules.Rules()
        rule_to_use = 'CloseLowerThanPrev'
        vpa_rules.add_symbol_to_rule(
                symbol = symbol_data,
                rules = rules,
                rule = rule_to_use)
        expected_result = vpa_rules.Rules(CloseLowerThanPrev = [
            helpers.Symbol(
                name = 'MOQ LIMITED', 
                code = 'MOQ', 
                industry = 'Services & Technology', 
                date_au = '01/02/2020', 
                open = 5.0, 
                high = 5.1, 
                low = 2.6, 
                close = 9.0, 
                volume = 100, 
                weekday_au = 'Mon', 
                date_utc_unixtimestamp = 123, 
                prev_open = 1.0, 
                prev_high = 1.0, 
                prev_low = 1.0, 
                prev_close = 1.0, 
                prev_volume = 1, 
                update_unixtimestamp = 1)],
            SidewaysPriceForLast3Intervals = [])
        # Interestin finding, if this test has an empty list rule, it can be omitted and test still passes! hmm
        self.assertEqual(expected_result, rules)
