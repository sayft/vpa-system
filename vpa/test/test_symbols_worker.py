import unittest
import logging
import os
import sys

from operator import attrgetter
from vpa import helpers
from vpa import symbols_worker

class TestSymbols(unittest.TestCase):

    ont_data = [
            helpers.Symbol(name = '1300 SMILES LIMITED', code ='ONT', industry = 'Tech', date_au = '03/05/2020', open = 10.0, high = 12.0, low = 9.5, close = 11.0, 
                volume = 1000, weekday_au = 'Sun', date_utc_unixtimestamp = 123),
            helpers.Symbol(name = '1300 SMILES LIMITED', code ='ONT', industry = 'Tech', date_au = '15/05/2020', open = 12.0, high = 12.0, low = 5.5, close = 9.0, 
                volume = 999, weekday_au = 'Fri', date_utc_unixtimestamp = 125),
            helpers.Symbol(name = '1300 SMILES LIMITED', code ='ONT', industry = 'Tech', date_au = '17/01/2020', open = 4.0, high = 90.0, low = 54.5, close = 74.0,
                volume = 27, weekday_au = 'Wed', date_utc_unixtimestamp = 5)]

    def test_get_prev_value(self):
        
        field_to_get_prev_value_from = 'volume'
        expected_result = [1000, 27, 0]
        actual_result = symbols_worker.get_prev_value(symbol = sorted(self.ont_data, key = attrgetter('date_utc_unixtimestamp'), reverse = True), field_to_work_on = field_to_get_prev_value_from)

        self.assertEqual(actual_result, expected_result)
    
    def test_get_prev_value_empty(self):

        input_data = [helpers.Symbol()]
        field_to_get_prev_value_from = 'volume'
        actual_result = symbols_worker.get_prev_value(symbol = input_data, field_to_work_on = field_to_get_prev_value_from)
        expected_result = [0]

        self.assertEqual(actual_result, expected_result)


    def test_set_prev_values(self):
        sorted_data = sorted(self.ont_data, key = attrgetter('date_utc_unixtimestamp'), reverse = True)
        prev_value_field_name = 'prev_volume'
        prev_volume_data = [1,2,3]
        updated_data = symbols_worker.set_prev_val(symbol = sorted_data, prev_values = prev_volume_data, field_to_update = prev_value_field_name)
        updated_data_prev_volume = []
        for row in range(0, len(updated_data)):
            updated_data_prev_volume.append(updated_data[row].__getattribute__(prev_value_field_name))
        self.assertEqual(updated_data_prev_volume, prev_volume_data)


if __name__ == '__main__':
    unittest.main()
