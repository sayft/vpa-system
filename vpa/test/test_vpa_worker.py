import unittest
import logging
import mypy

from vpa import helpers, vpa_rules, vpa_worker
from collections import namedtuple


class TestVpaWorker(unittest.TestCase):

    dynamicRules = namedtuple('dynamicRules', 'name arguments')

    def test_monotonic_decrease(self) -> None:
        sample_data = [1,2,3,4,5]
        expected_result = True
        actual_result = vpa_worker.last_n_monotonic_operation(symbol = sample_data, num_intervals = 5, operation = 'decreasing')

        self.assertEqual(expected_result, actual_result)

    def test_monotonic_increase(self) -> None:
        sample_data = [5,4,3,2,1]
        expected_result = True
        actual_result = vpa_worker.last_n_monotonic_operation(symbol = sample_data, num_intervals = 5, operation = 'increasing')
        
        self.assertEqual(expected_result, actual_result)

    def test_non_monotonic_decrease(self) -> None:
        sample_data = [1,2,2,1,5]
        expected_result = False
        actual_result = vpa_worker.last_n_monotonic_operation(symbol = sample_data, num_intervals = 5, operation = 'decreasing')

        self.assertFalse(expected_result, actual_result)

    def test_non_monotonic_increase(self) -> None:
        sample_data = [5,1,2,2,1]
        expected_result = False
        actual_result = vpa_worker.last_n_monotonic_operation(symbol = sample_data, num_intervals = 5, operation = 'increasing')

        self.assertFalse(expected_result, actual_result)
