import unittest
import logging
import os
import datetime
import calendar
import sys
import mypy

from typing import Sequence 
from pytz import timezone
from vpa import helpers

logging.basicConfig(level=logging.WARNING)

class TestHelpers(unittest.TestCase):

    def test_read_file(self):
        file_contents =  helpers.read_file(os.path.join("./vpa/test/input/input_file.txt"))
        self.assertEqual(file_contents, "Hello World!\n")
    
    def test_get_symbols_meta(self) -> None:
        asx_file = "./vpa/test/input/asx_symbols.csv"

        expected_result = [
                helpers.Symbol_meta(name='MOQ LIMITED', code='MOQ', industry='Software & Services'), 
                helpers.Symbol_meta(name='1300 SMILES LIMITED', code='ONT', industry='Health Care Equipment & Services'), 
                helpers.Symbol_meta(name='1414 DEGREES LIMITED', code='14D', industry='Capital Goods'), 
                helpers.Symbol_meta(name='1ST GROUP LIMITED', code='1ST', industry='Health Care Equipment & Services'), 
                helpers.Symbol_meta(name='333D LIMITED', code='T3D', industry='Commercial & Professional Services')]
        actual_result = helpers.get_symbols_meta(asx_file)

        self.assertEqual(actual_result, expected_result)

    def test_set_symbols_meta(self) -> None:

        asx_file = "./vpa/test/input/asx_symbols.csv"
        symbols_meta = helpers.get_symbols_meta(asx_file)
        moq_meta = symbols_meta[0]
        moq_data = [
                helpers.Symbol('DEFAULT', 'DEF', 'DEFAULT', '01/01/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0),
                helpers.Symbol('DEFAULT', 'DEF', 'DEFAULT', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0),
                helpers.Symbol('DEFAULT', 'DEF', 'DEFAULT', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0)]

        actual_result = helpers.set_symbols_meta(symbol_meta = moq_meta, symbol = moq_data)
        expected_result = [
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '01/01/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0),
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0),
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0)]

        self.assertEqual(actual_result, expected_result)

    def test_get_number_of_elements(self) -> None:

        input  = 'name varchar(255),\ncode varchar(255),\nindustry varchar(255),\ndate_au varchar(255),\nopen double,\nhigh double,\nlow double,\nclose double,\nvolume double,\nweekday_au varchar(255),\ndate_utc_unixtimestamp double,\nprev_volume double,\nPRIMARY KEY(date_au)\n'

        expected_result = 12
        actual_result = helpers.get_number_of_elements(data = input, num_cols_to_omit = 1)

        self.assertEqual(expected_result, actual_result)

    def test_get_column_names(self) -> None:

        input = 'name varchar(255),\ncode varchar(255),\nindustry varchar(255),\ndate_au varchar(255),\nopen double,\nhigh double,\nlow double,\nclose double,\nvolume double,\nweekday_au varchar(255),\ndate_utc_unixtimestamp double,\nprev_volume double,\nPRIMARY KEY(date_au)\n'
        expected_result = [
                'name',
                'code',
                'industry',
                'date_au',
                'open',
                'high',
                'low',
                'close',
                'volume',
                'weekday_au',
                'date_utc_unixtimestamp',
                'prev_volume']
        actual_result = helpers.get_column_names(data = input, num_cols_to_omit = 2)

        self.assertEqual(expected_result, actual_result)

    def test_get_symbol_data_from_csv(self) -> None:
        data = [
                ['2020-03-27', '12.9500', '15.6100', '12.8000', '15.5500', '6603567'],
                ['2020-03-26', '14.4500', '14.5000', '13.3900', '13.9700', '5984995'],
                ['2020-03-25', '14.7800', '16.6000', '14.0400', '14.8500', '5474689']]
        data_with_header = data
        data_with_header.insert(0, ['timestamp' ,'open', 'high', 'low', 'close', 'volume'])
        expected_result = [
                helpers.Symbol('DEFAULT', 'DEF', 'Default Industry', '2020-03-27', 12.9500, 15.6100, 12.8000, 15.5500, 6603567, 'Mon', 0, 1.0),
                helpers.Symbol('DEFAULT', 'DEF', 'Default Industry', '2020-03-26', 14.4500, 14.5000, 13.3900, 13.9700, 5984995, 'Mon', 0, 1.0),
                helpers.Symbol('DEFAULT', 'DEF', 'Default Industry', '2020-03-25', 14.7800, 16.6000, 14.0400, 14.8500, 5474689, 'Mon', 0, 1.0)]
        actual_result = helpers.get_symbol_data_from_csv(csv_data = data)
        actual_result_with_header_input = helpers.get_symbol_data_from_csv(csv_data = data_with_header)

        self.assertEqual(expected_result, actual_result)
        self.assertEqual(expected_result, actual_result_with_header_input)

    def test_get_symbol_data_from_csv_with_cast_errors(self) -> None:
        with self.assertLogs(level='ERROR') as log:
            data = [['2020-03-27', '12.9500', 'THIS_SHOULD_NOT_BE_CAST_TO_FLOAT', '12.8000', '15.5500', '6603567']]
            actual_result = helpers.get_symbol_data_from_csv(csv_data = data)

            self.assertIn("An error occured during cast attempts from an API return to Symbol namedtuple for data", log.output[0])

    def test_get_common_records(self) -> None:
        symbol_data = [
                helpers.Symbol()._replace(date_au = '01/02/2020'),
                helpers.Symbol()._replace(date_au = '05/06/2020')]
        sql_output = [
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100.0, 'Mon', 123, 0.0, 0.0, 0.0, 0.0, 1, 10],
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100.0, 'Mon', 123, 0.0, 0.0, 0.0, 0.0, 1, 10]]
        sql_symbol = helpers.get_symbol_data_from_sql(sql_result = sql_output)
        expected_result = ['01/02/2020']
        actual_result = helpers.get_common_records(
                symbol = symbol_data, 
                query = sql_symbol,
                column_to_compare = 'date_au')
        self.assertEqual(expected_result, actual_result)

    def test_get_common_values_no_common_values(self) -> None:
        symbol_data = [
                helpers.Symbol()._replace(date_au = '01/02/2020'),
                helpers.Symbol()._replace(date_au = '05/06/2020')]
        sql_output = [
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '1/04/2020', 5.0, 5.1, 2.6, 9.0, 100.0, 'Mon', 123, 0.0, 0.0, 0.0, 0.0, 1, 10],
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '31/03/2020', 5.0, 5.1, 2.6, 9.0, 100.0, 'Mon', 123, 0.0, 0.0, 0.0, 0.0, 1, 10]]
        sql_symbol = helpers.get_symbol_data_from_sql(sql_result = sql_output)
        expected_result = [] # type: Sequence[str]
        actual_result = helpers.get_common_records(
                symbol = symbol_data, 
                query = sql_symbol,
                column_to_compare = 'date_au')
        
        self.assertEqual(expected_result, actual_result)
    
    def test_get_common_values_no_sql_result(self) -> None:
        with self.assertLogs(level='INFO') as log:
            symbol_data = [
                    helpers.Symbol()._replace(date_au = '01/02/2020'),
                    helpers.Symbol()._replace(date_au = '05/06/2020')]
            sql_output = [] # type: Sequence[str]
            sql_symbol = helpers.get_symbol_data_from_sql(sql_result = sql_output)
            expected_result = [] # type: Sequence[str]
            actual_result = helpers.get_common_records(
                    symbol = symbol_data, 
                    query = sql_symbol,
                    column_to_compare = 'date_au')
         
            self.assertIn("No data found in SQL result", log.output[0])
            self.assertEqual(expected_result, actual_result)
            
    def test_get_filtered_data_based_on_values(self) -> None:
        symbol_data = [
                helpers.Symbol()._replace(date_au = '01/02/2020'),
                helpers.Symbol()._replace(date_au = '05/06/2020')]
        common_values_data = ['01/02/2020']
        expected_result = [
                helpers.Symbol('DEFAULT', 'DEF', 'Default Industry', '05/06/2020', 1.0, 1.0, 1.0, 1.0, 1, 'Mon', 0, 1.0, 1.0, 1.0, 1.0, 1, 1)]
        actual_result = helpers.get_filtered_data_based_on_common_values(
                symbol = symbol_data, 
                common_values = common_values_data,
                column_to_filter = 'date_au')
        
        self.assertEqual(expected_result, actual_result)

    def test_get_filtered_data_based_on_common_values_with_empty_common_values(self) -> None:
        symbol_data = [
                helpers.Symbol()._replace(date_au = '01/02/2020'),
                helpers.Symbol()._replace(date_au = '05/06/2020')]
        common_values_data = [] # type: Sequence[str]
        expected_result = [
                helpers.Symbol('DEFAULT', 'DEF', 'Default Industry', '01/02/2020', 1.0, 1.0, 1.0, 1.0, 1, 'Mon', 0, 1.0, 1.0, 1.0, 1.0, 1, 1),
                helpers.Symbol('DEFAULT', 'DEF', 'Default Industry', '05/06/2020', 1.0, 1.0, 1.0, 1.0, 1, 'Mon', 0, 1.0, 1.0, 1.0, 1.0, 1, 1)]
        actual_result = helpers.get_filtered_data_based_on_common_values(
                symbol = symbol_data, 
                common_values = common_values_data,
                column_to_filter = 'date_au')

        self.assertEqual(expected_result, actual_result)
        
    def test_get_symbol_data_from_sql(self) -> None:

        sql = [
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0,  0.0, 0.0, 0.0, 1, 10],
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0,  0.0, 0.0, 0.0, 1, 10],
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '10/05/2020', 15.0, 15.1, 12.6, 19.0, 100, 'Mon', 123, 0,  0.0, 0.0, 0.0, 1, 10]]
        expected_result = [
                helpers.Symbol(name='MOQ LIMITED', code='MOQ', industry='Services & Technology', date_au='01/02/2020', 
                    open=5.0, high=5.1, low=2.6, close=9.0, volume=100, weekday_au='Mon', 
                    date_utc_unixtimestamp=123, prev_open=0.0, prev_high=0.0, prev_low=0.0, 
                    prev_close=0.0, prev_volume=1, update_unixtimestamp=10), 
                helpers.Symbol(name='MOQ LIMITED', code='MOQ', industry='Services & Technology', date_au='01/03/2020', 
                    open=5.0, high=5.1, low=2.6, close=9.0, volume=100, weekday_au='Mon', 
                    date_utc_unixtimestamp=123, prev_open=0.0, prev_high=0.0, prev_low=0.0, 
                    prev_close=0.0, prev_volume=1, update_unixtimestamp=10), 
                helpers.Symbol(name='MOQ LIMITED', code='MOQ', industry='Services & Technology', date_au='10/05/2020', 
                    open=15.0, high=15.1, low=12.6, close=19.0, volume=100, weekday_au='Mon', 
                    date_utc_unixtimestamp=123, prev_open=0.0, prev_high=0.0, prev_low=0.0, prev_close=0.0, prev_volume=1, update_unixtimestamp=10)]
        actual_result = helpers.get_symbol_data_from_sql(sql_result = sql)

        self.assertEqual(expected_result, actual_result)
    
    def test_get_symbol_data_from_empty_sql(self) -> None:
        with self.assertLogs(level='WARN') as log:
            sql = [] # type: Sequence[str]
            actual_result = helpers.get_symbol_data_from_sql(sql_result = sql)
            self.assertIn("No data found in SQL result", log.output[0])
            self.assertEqual(sql, actual_result)
    
    #Everyone's favourite time and date tests!!! :<
    def test_get_unixtimestamp_from_aus_date(self) -> None:
        aus_date_inside_dst = "2019-12-23"
        aus_date_outside_dst = "2020-04-28"

        expected_result_inside_dst = 1577019600
        expected_result_outside_dst = 1587996000

        timezone_format = 'Australia/Sydney'
        date_format = '%Y-%m-%d'


        actual_result_inside_dst = helpers.get_unixtimestamp_from_aus_date(
                date = aus_date_inside_dst,
                timezone = timezone_format,
                format = date_format)
        actual_result_outside_dst =  helpers.get_unixtimestamp_from_aus_date(
                date = aus_date_outside_dst,
                timezone = timezone_format,
                format = date_format)
        
        self.assertEqual(expected_result_inside_dst, actual_result_inside_dst)
        self.assertEqual(expected_result_outside_dst, actual_result_outside_dst)

        #convert back to datetime

        self.assertEqual(datetime.datetime.strftime(datetime.datetime.fromtimestamp(1587996000, tz = timezone(timezone_format)), date_format), aus_date_outside_dst)
        self.assertEqual(datetime.datetime.strftime(datetime.datetime.fromtimestamp(1577019600, tz = timezone(timezone_format)), date_format), aus_date_inside_dst)

    def test_set_unixttimestamp(self) -> None:
        timezone_format = 'Australia/Sydney'
        date_format = '%Y-%m-%d'
        input = [helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '2020-04-28', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 1.0)]
        expected_result = [helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '2020-04-28', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 1587996000, 1.0, 1.0, 1.0, 1.0, 1, 1)]
        actual_result = helpers.set_unixtimestamp_from_aus_date(
                symbol = input,
                timezone_format = timezone_format,
                date_format = date_format)

        self.assertEqual(expected_result, actual_result)

    def test_update_unixtimestamp(self) -> None:
        input = [helpers.Symbol()]

        unixtime = 1588666012
        expected_result = [helpers.Symbol()._replace(update_unixtimestamp = unixtime)]
        actual_result = helpers.update_unixtimestamp(symbol = input, unixtimestamp = unixtime)
        
        self.assertEqual(expected_result, actual_result)

    def test_update_day_of_the_week(self) -> None:
        input = [helpers.Symbol()._replace(date_au = '2020-01-01')]
        expected_result = [helpers.Symbol()._replace(date_au = '2020-01-01', weekday_au = 'Wednesday')]
        actual_result = helpers.update_day_of_the_week(
                symbol = input,
                date_format = '%Y-%m-%d')
        
        self.assertEqual(expected_result, actual_result)

    def test_get_largest_value(self) -> None:
        input = [
                helpers.Symbol()._replace(date_utc_unixtimestamp = 123),
                helpers.Symbol()._replace(date_utc_unixtimestamp = 1234)]
        expected_result = 1234
        actual_result = helpers.get_largest_value(symbol = input, column_to_use = 'date_utc_unixtimestamp')

        self.assertEqual(expected_result, actual_result)

    def test_get_largest_value_no_input(self) -> None:
        with self.assertLogs(level='ERROR') as log:
            input = [] # type: Sequence[str]
            expected_result = 'No maximum value to find for date_utc_unixtimestamp in Symbol list: []'
            actual_result = helpers.get_largest_value(symbol = input, column_to_use = 'date_utc_unixtimestamp')

            self.assertIn(expected_result, log.output[0])

    def test_table_manipulation_should_be_update_as_api_is_fresher(self) -> None:
        symbol_input = [
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '2020-01-01', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0),
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '2020-01-27', 5.0, 5.1, 2.6, 9.0, 123, 'Mon', 1234, 0)]
        sql_output = [
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '2020-01-01', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 10, 0, 0.0, 0.0, 0.0, 1, 10],
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '2020-01-27', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 1234, 0, 0.0, 0.0, 0.0, 1, 10]]
        sql_symbol = helpers.get_symbol_data_from_sql(sql_result = sql_output)
        expected_result = 'Update'
        actual_result = helpers.table_manipulation_decision(
                symbol = symbol_input,
                sql_result = sql_symbol,
                column_to_compare = 'date_utc_unixtimestamp')

        self.assertEqual(expected_result, actual_result)

    def test_table_manupulation_should_be_skipped_as_api_and_sql_are_the_same(self) -> None:
        symbol_input = [
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '2020-01-01', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0),
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '2020-01-27', 5.0, 5.1, 2.6, 9.0, 123, 'Mon', 1234, 0)]
        sql_symbol = symbol_input
        expected_result = 'Skip'
        actual_result = helpers.table_manipulation_decision(
                symbol = symbol_input,
                sql_result = sql_symbol,
                column_to_compare = 'date_utc_unixtimestamp')

        self.assertEqual(expected_result, actual_result)

    def test_table_manipulation_should_be_write_as_api_is_new(self) -> None:
        symbol_input = [
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '2020-01-01', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0),
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '2020-01-27', 5.0, 5.1, 2.6, 9.0, 123, 'Mon', 1234, 0)]
        sql_output = [
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '2020-01-01', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 0)]
        expected_result = 'Write'
        sql_symbol = helpers.get_symbol_data_from_sql(sql_result = sql_output)
        actual_result = helpers.table_manipulation_decision(
                symbol = symbol_input,
                sql_result = sql_symbol,
                column_to_compare = 'date_utc_unixtimestamp')

        self.assertEqual(expected_result, actual_result)

    def test_table_manipulation_should_be_write_as_sql_is_empty(self) -> None:
        symbol_input = [helpers.Symbol('MOQ LIMITED', 'MOQ', 'Software & Services', '2020-01-27', 5.0, 5.1, 2.6, 9.0, 123, 'Mon', 1234, 0)]
        sql_symbol = [] # type: Sequence[str]
        expected_result = 'Write'
        actual_result = helpers.table_manipulation_decision(
                symbol = symbol_input,
                sql_result = sql_symbol,
                column_to_compare = 'date_utc_unixtimestamp')

        self.assertEqual(expected_result, actual_result)

    def test_get_leftover_items(self) -> None:
        current_table_data = ['asx_ont_daily_stg', 'asx_1d9_daily_stg']
        update_table_data = ['asx_ont_daily_stg', 'asx_1d9_daily_stg', 'asx_abc_daily_stg']

        actual_result = helpers.get_leftover_items(data1=current_table_data, data2=update_table_data)
        expected_result = ['asx_abc_daily_stg']

        self.assertEqual(expected_result, actual_result)


if __name__ == '__main__':
    unittest.main()
