import argparse
import logging
import os

from collections import namedtuple
from datetime import date
from database import database_worker
from itertools import groupby
from operator import attrgetter
from vpa import helpers, vpa_rules, vpa_worker


def main():

    rulesToCheck = namedtuple('rulesToCheck', 'name_on_file rule')
    dynamicRules = namedtuple('dynamicRules', 'name arguments')

    parser = argparse.ArgumentParser(description = "Sift through table data, figure out which symbol belongs to which rule(s) and save output to file")
    parser.add_argument("--db_name", type = str, help = "Database name from db_connection object")
    parser.add_argument("--db_host", type = str, help = "Database host server value, i.e. 127.0.0.1")
    parser.add_argument("--db_socket", type = str, help = "Database socket, i.e. /tmp/mysql.sock")
    parser.add_argument("--symbols_file", type = str, help = "Path and name of symbol file.")
    parser.add_argument("--table_suffix", type = str, help = "Parse table suffix that will be used for grabbing data for a symbol from table, i.e. _daily_staging")
    parser.add_argument("--max_identifier", type = str, help = "Symbol namedtuple column to use as max value identifier for latest record return")
    parser.add_argument("--output_dir", type = str, help = "Output directory where the output file will be written to")

    args = parser.parse_args()
    date_today = date.today().strftime("%d%m%Y")
    log_dir = os.path.join("./logs")
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(
            filename = os.path.join(log_dir, "vpa_rules{date_today_val}.log".format(date_today_val = date_today)),
            format = '%(asctime)s %(levelname)-1s %(message)s',
            level = logging.INFO,
            datefmt = '%d-%m-%Y %H:%M:%S')
    logger = logging.getLogger(__name__)
    input_dir = os.path.join("./input")

    db_user = os.environ['DB_USER']
    db_password = os.environ['DB_USER_PASSWORD']
    if not db_user:
        logger.error("DB_USER environment variable is not set or empty, can not proceed.")
        exit(1)
    if not db_password:
        logger.error("DB_USER_PASSWORD environment variable is not set or empty. can not proceed.")
        exit(1)

    db_con = database_worker.set_connection_pymysql(
            database = args.db_name,
            username = db_user,
            password = db_password,
            host = args.db_host,
            socket = args.db_socket)

    asx_companies = helpers.get_symbols_meta(os.path.join(args.symbols_file))
    industries = set([company.industry for company in asx_companies])
    rules = vpa_rules.Rules()

    logging.info("Beginning VPA rule generation for {date_today_val}".format(date_today_val = date_today))
    for company in asx_companies:
        table_name = "asx_{code_val}{table_suffix_val}".format(
                code_val = company.code.lower(),
                table_suffix_val = args.table_suffix.lower())
        table_data = database_worker.select_from_table(table = table_name, connection = db_con)
        table_symbol = helpers.get_symbol_data_from_sql(table_data)
        if not table_symbol:
            logging.error("SQL result is empty for {table_val}, this table will be skipped".format(table_val = table_name))
            logging.info("---------------------------------------------")
            continue
        table_symbol = sorted(table_symbol, key=attrgetter('date_utc_unixtimestamp'), reverse = True)
        # Write out VPA rules for which Symbols will be checked against
        rules_to_check = [rulesToCheck(
                                       name_on_file='MonotonicDecreaseLowPriceLowVolOverNPeriods',
                                       rule=[
                                             dynamicRules(
                                                 name=vpa_worker.last_n_monotonic_operation,
                                                 arguments={
                                                     'symbol': [vol.volume for vol in table_symbol],
                                                     'num_intervals': 3,
                                                     'operation': 'decreasing'}),
                                             dynamicRules(
                                                 name=vpa_worker.last_n_monotonic_operation,
                                                 arguments={
                                                     'symbol': [l.low for l in table_symbol],
                                                     'num_intervals': 3,
                                                     'operation': 'decreasing'})]
                                     ),
                          rulesToCheck(
                                       name_on_file='MonotonicDecreaseLowPriceIncreaseVoluOverNPeriods',
                                       rule=[
                                             dynamicRules(
                                                 name=vpa_worker.last_n_monotonic_operation,
                                                 arguments={
                                                     'symbol': [vol.volume for vol in table_symbol],
                                                     'num_intervals': 3,
                                                     'operation': 'increasing'}),
                                             dynamicRules(
                                                 name=vpa_worker.last_n_monotonic_operation,
                                                 arguments={
                                                     'symbol': [l.low for l in table_symbol],
                                                     'num_intervals': 3,
                                                     'operation': 'decreasing'})])
                         ]
        # For each rule, go over the rule with symbol data as input, if the return is true, add that symbol to the rule
        logging.info("Check rule membership for {symbol_name}".format(symbol_name=table_symbol[0].code))
        for outer_rule in rules_to_check:
            rule_result = []
            symbols_belonging_to_rule = []
            for inner_rule in outer_rule.rule:
                if inner_rule.name(**inner_rule.arguments):
                    rule_result.append(True)
                else:
                    rule_result.append(False)
            #print("Symbol: {symbol}. result: {result}".format(result=rule_result, symbol=table_symbol[0].code))
            if all(rule_result):
                vpa_rules.add_symbol_to_rule(
                    symbol = table_symbol[0],
                    rules = rules,
                    rule = outer_rule.name_on_file)
    logging.info("Rule membership for all symbols checked")
    #print("Vpa_rules: {vpa_rule}".format(vpa_rule=rules))
    
    # separate rules into dict so that you can use rule name in logic
    rules_dict = rules._asdict()
    # remove any empty rules
    valid_rules = {key: value for key, value in rules_dict.items() if value}
    logging.info("Preparing rules for file writing")
    for rule_name, rule_items in valid_rules.items():
        rule_sorter = sorted(rule_items, key=attrgetter('industry'))
        industry_grouper = groupby(rule_sorter, key=attrgetter('industry'))

        symbols_by_industry = {industry: list(map(attrgetter('code'), symbols)) for industry, symbols in industry_grouper}
        logging.info("Writing out file for rule: {rule_name}".format(rule_name=rule_name))
        with open(os.path.join(args.output_dir, "{rule_name}.txt".format(rule_name=rule_name)), 'w') as f:
            f.write("VPA Rules Generated for {date_today_val}\n".format(date_today_val = date.today().strftime("%d-%m-%Y"))) 
            f.write("\nNone of the information here on this website should be treated as financial advice. This is a purely educational system.\nAny financial losses or gains based on the information provided here is on you. Do your own research before entering the market\n\n\n\n")
            f.write("----------------------------------------------\n")

            for industry, symbols in symbols_by_industry.items():
                f.write("{industry_val} | {symbols_val}\n".format(industry_val=industry, symbols_val=" ".join(symbols))) 
            f.write("----------------------------------------------\n")
        f.close()

    logging.info("VPA rule generation complete for {date_today_val}".format(date_today_val = date.today().strftime("%d-%m-%Y")))

if __name__ == '__main__':
    main()
