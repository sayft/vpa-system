from vpa import helpers
from database import database_worker as dw

from typing import List


def get_latest_table_names(tables_file, table_prefix, table_suffix) -> List:
    """
    Given a table file that represents ASX companies, get the Symbol names
    and return a list of table names using Symbol name, suffix and prefix as
    table name builders.
    """

    table_data = helpers.get_symbols_meta(full_file_path=tables_file)
    table_names = [table_prefix + table.code.lower() + table_suffix for table in table_data]

    return table_names

       
