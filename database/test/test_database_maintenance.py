import unittest
import os
import logging
import mypy


from database import database_maintenance as dm
from vpa import helpers

logging.basicConfig(level=logging.INFO)

class TestDatabaseMaintenance(unittest.TestCase):

    def test_get_latest_table_names(self) -> None:
        actual_result = dm.get_latest_table_names(
	    tables_file=os.path.join('database/test/input/unittest_table_names.txt'),
	    table_prefix='asx_',
	    table_suffix='_daily_test')
        expected_result = ['asx_moq_daily_test', 'asx_ont_daily_test', 'asx_14d_daily_test', 'asx_1st_daily_test', 'asx_t3d_daily_test', 'asx_tdi_daily_test']

        self.assertEqual(actual_result[0], expected_result[0])





if __name__ == "__main__":
    unittest.main()
