import unittest
import os
import logging
import mypy

from database import database_worker
from vpa import helpers, symbols_worker
from typing import List

logging.basicConfig(level=logging.INFO)

class TestDatabaseWorker(unittest.TestCase):

    def get_connection(self):
        db_user = os.environ['DB_USER_UNITTEST']
        db_password = os.environ['DB_USER_UNITTEST_PASSWORD']
        if not db_user:
            logging.error("DB_USER_UNITTEST environment variable is not set or empty, can not proceed.")
            exit(1)
        if not db_password:
            logging.error("DB_USER_UNITTEST_PASSWORD environment variable is not set or empty. can not proceed.")
            exit(1)
        db_conn = database_worker.set_connection_pymysql(
                database = "unittest",
                username = db_user,
                password = db_password,
                host = "127.0.0.1",
                socket = "/tmp/mysql.sock")
        return db_conn


    def test_connection(self):
        db_connection = TestDatabaseWorker.get_connection(self)
        self.assertEqual(db_connection.open, True)

    def test_create_table(self):
        db_connection = TestDatabaseWorker.get_connection(self)
        table_struct = database_worker.read_table_info("./database/test/input/unittest_table_structure.txt")
        expected_table = "asx_unittest_abc_daily_ut"
        
        database_worker.drop_table(table = expected_table, connection = db_connection)
        database_worker.create_table(
                table = "asx_unittest_abc",
                freq = "daily",
                env = "ut",
                connection = db_connection,
                table_structure = table_struct)
        cursor = db_connection.cursor()
        cursor.execute("SHOW TABLES")
        show_tables = cursor.fetchall()
        cursor.close()
        tables_in_db = [item[0] for item in show_tables]
        database_worker.drop_table(table = expected_table, connection = db_connection)

        self.assertEqual(expected_table in tables_in_db, True)

    def test_drop_table(self):
        logging.info("test_drop_table")
        db_connection = TestDatabaseWorker.get_connection(self)
        table_struct = database_worker.read_table_info(os.path.join("./database/test/input/unittest_table_structure.txt"))
        expected_table = "asx_unittest_should_not_exist_daily_ut"

        database_worker.create_table(
                table = "asx_unittest_should_not_exist",
                freq = "daily",
                env = "ut",
                connection = db_connection,
                table_structure = table_struct)

        cursor = db_connection.cursor()
        cursor.execute("SHOW TABLES")
        show_tables_before = cursor.fetchall()
        tables_before = [item[0] for item in show_tables_before]
        
        database_worker.drop_table(expected_table, db_connection)
        cursor.execute("SHOW TABLES")
        show_tables_after = cursor.fetchall()
        tables_after = [item[0] for item in show_tables_after]

        self.assertEqual(expected_table in tables_before, True)
        self.assertEqual(expected_table not in tables_after, True)

    def test_write_table(self) -> None:
        logging.info("test_write_table")
        db_connection =  TestDatabaseWorker.get_connection(self)
        table_struct = database_worker.read_table_info("./database/test/input/unittest_table_structure.txt")
        input_data = [
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123),
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123)]

        table_name = "asx_unittest_yoc_daily_ut"
        logging.info("Dropping {table_val}".format(table_val = table_name))
        database_worker.drop_table(table = table_name, connection = db_connection)
        database_worker.create_table(
                table = "asx_unittest_yoc",
                freq = "daily",
                env = "ut",
                connection = db_connection,
                table_structure = table_struct)
        database_worker.write_to_table(
                table = table_name, 
                connection = db_connection,
                data = input_data,
                table_structure = table_struct)
        table_data = database_worker.select_from_table(
                table = table_name,
                connection = db_connection)
        expected_result = [
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 1.0, 1.0, 1.0, 1.0, 1, 1], 
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 1.0, 1.0, 1.0, 1.0, 1, 1]]
        database_worker.drop_table(table = table_name, connection = db_connection)
        
        self.assertEqual(expected_result, table_data)

    def test_write_empty_data_to_table(self) -> None:
        with self.assertLogs(level='WARNING') as log:
            db_connection =  TestDatabaseWorker.get_connection(self)
            table_struct = database_worker.read_table_info("./database/test/input/unittest_table_structure.txt")
            input_data = [] # type: List

            table_name = "asx_unittest_empty_daily_ut"
            database_worker.drop_table(table = table_name, connection = db_connection)
            database_worker.create_table(
                    table = "asx_unittest_empty",
                    freq = "daily",
                    env = "ut",
                    connection = db_connection,
                    table_structure = table_struct)
            database_worker.write_to_table(
                    table = table_name,
                    connection = db_connection,
                    data = input_data,
                    table_structure = table_struct)
            database_worker.drop_table(table = table_name, connection = db_connection)
            self.assertIn("No data to write", log.output[0])

    def test_partial_write_to_table_with_common_dates(self) -> None:
        with self.assertLogs(level='INFO') as log:
            db_connection = TestDatabaseWorker.get_connection(self)
            table_struct = database_worker.read_table_info("./database/test/input/unittest_table_structure.txt")
            # write initial data to table
            initial_input_data = [
                    helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123),
                    helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123)]
            table_name = "asx_unittest_moq_daily_ut"
            logging.info("Dropping {table_val}".format(table_val = table_name))
            database_worker.drop_table(table = table_name, connection = db_connection)
            database_worker.create_table(
                    table = "asx_unittest_moq",
                    freq = "daily",
                    env = "ut",
                    connection = db_connection,
                    table_structure = table_struct)
            database_worker.write_to_table(
                    table = table_name,
                    connection = db_connection,
                    data = initial_input_data,
                    table_structure = table_struct)
            # simulate extra data coming in from some sort of input
            extra_input_data = [
                    helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123),
                    helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123),
                    helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '10/05/2020', 15.0, 15.1, 12.6, 19.0, 100, 'Mon', 123)]
            table_output = database_worker.select_from_table(
                    table = table_name,
                    connection = db_connection)
            symbol_table = helpers.get_symbol_data_from_sql(sql_result = table_output)
            # filter data based on dates between extra input and what is already in the table
            filtered_data = symbols_worker.get_data_to_write_to_table(
                    symbol_data = extra_input_data, 
                    query_data = symbol_table,
                    filter_to_use = 'date_au')
            database_worker.write_to_table(
                    table = table_name,
                    connection = db_connection,
                    data = filtered_data,
                    table_structure = table_struct)
            expected_result = [
                    ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 1.0, 1.0, 1.0, 1.0, 1, 1], 
                    ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 1.0, 1.0, 1.0, 1.0, 1, 1], 
                    ['MOQ LIMITED', 'MOQ', 'Services & Technology', '10/05/2020', 15.0, 15.1, 12.6, 19.0, 100, 'Mon', 123, 1.0, 1.0, 1.0, 1.0, 1, 1]]
            data_from_updated_table = database_worker.select_from_table(
                    table = table_name,
                    connection = db_connection)
            # simulate the same data being entered into table
            same_data = symbols_worker.get_data_to_write_to_table(
                    symbol_data = initial_input_data, 
                    query_data = symbol_table,
                    filter_to_use = 'date_au')
            database_worker.write_to_table(
                    table = table_name,
                    connection = db_connection,
                    data = same_data,
                    table_structure = table_struct)
            database_worker.drop_table(table = table_name, connection = db_connection)
            self.assertIn("Writing 1 rows to {table_name_val}".format(table_name_val = table_name), log.output[6])
            self.assertIn("No data to write into {table_name_val}".format(table_name_val = table_name), log.output[8])
            
            self.assertEqual(expected_result, data_from_updated_table)

    def test_delete_date_data(self) -> None:
        db_connection = TestDatabaseWorker.get_connection(self)
        table_struct = database_worker.read_table_info("./database/test/input/unittest_table_structure.txt")
        # write initial data to table
        initial_input_data = [
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123),
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123)]
        table_name = "asx_unittest_moq_daily_ut"
        logging.info("Dropping {table_val}".format(table_val = table_name))
        database_worker.drop_table(table = table_name, connection = db_connection)
        database_worker.create_table(
                table = "asx_unittest_moq",
                freq = "daily",
                env = "ut",
                connection = db_connection,
                table_structure = table_struct)
        database_worker.write_to_table(table = table_name, connection = db_connection, data = initial_input_data,
                table_structure = table_struct)
        delete_this_date = '01/03/2020'
        database_worker.delete_from_table(table = table_name, connection = db_connection, date_to_delete = delete_this_date)
        data_from_table = database_worker.select_from_table(
                table = table_name,
                connection = db_connection)
        expected_result = [
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123, 1.0, 1.0, 1.0, 1.0, 1, 1]]

        self.assertEqual(expected_result, data_from_table)

    def test_update_table(self) -> None:
        db_connection = TestDatabaseWorker.get_connection(self)
        initial_input_data = [
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123),
                helpers.Symbol('MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 100, 'Mon', 123)]
        table_struct = database_worker.read_table_info("./database/test/input/unittest_table_structure.txt")
        table_name = "asx_unittest_moq_daily_ut"
        logging.info("Dropping {table_val}".format(table_val = table_name))
        database_worker.drop_table(table = table_name, connection = db_connection)
        database_worker.create_table(
                table = "asx_unittest_moq",
                freq = "daily",
                env = "ut",
                connection = db_connection,
                table_structure = table_struct)
        database_worker.write_to_table(table = table_name, connection = db_connection, data = initial_input_data,
                table_structure = table_struct)
        data_from_table = database_worker.select_from_table(
                table = table_name,
                connection = db_connection)
        update_input = helpers.Symbol(
                'MOQ LIMITED', 'MOQ', 'Services & Technology', 
                '01/02/2020', 123.0, 123.0, 123.0, 123.0, 
                9999, 'Mon', 99999, 123.0, 123.0, 123.0, 99999.0, 123, 1589274544)
        database_worker.update_table(table = table_name, connection = db_connection, 
                symbol = update_input)
        updated_data_from_table = database_worker.select_from_table(
                table = table_name,
                connection = db_connection)
        expected_original_result = [
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 5.0, 5.1, 2.6, 9.0, 
                    100, 'Mon', 123, 1.0, 1.0, 1.0, 1.0, 1, 1], 
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 
                    100, 'Mon', 123, 1.0, 1.0, 1.0, 1.0, 1, 1]]
        expected_updated_result = [
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/02/2020', 123.0, 123.0, 123.0, 
                    123.0, 9999., 'Mon', 99999.0, 123.0, 123.0, 123.0, 99999.0, 123, 1589274544], 
                ['MOQ LIMITED', 'MOQ', 'Services & Technology', '01/03/2020', 5.0, 5.1, 2.6, 9.0, 
                    100, 'Mon', 123.0, 1.0, 1.0, 1.0, 1.0, 1, 1]]

        self.assertEqual(expected_original_result, data_from_table)
        self.assertEqual(expected_updated_result, updated_data_from_table)


if __name__ == '__main__':
    unittest.main()
