import os
import pymysql
import logging
import mypy
import mysql.connector # type: ignore

from typing import List
from vpa import helpers

logging.basicConfig(
        format = '%(asctime)s %(levelname)-1s %(message)s',
        level = logging.WARN,
        datefmt = '%d-%m-%Y %H:%M:%S')

def set_connection_pymysql(database, username, password, host, socket):
    try:
        logging.info("Establishing connection to {database_val} as {username_val} with host {host_val} using socket {socket_val}".format(
            database_val = database, username_val = username, host_val = host, socket_val = socket))
        conn = pymysql.connect(host = host, unix_socket = socket, user = username, passwd = password, db = database)
        
        return conn
    except pymysql.err.OperationalError as error:
        logging.error("Error, please check your connection settings: database={database_val}, user={username_val}, host={host_val}, socket={socket_val}\n {error_val}".format(
            database_val = database, 
            username_val = username, 
            host_val = host, 
            socket_val = socket,
            error_val = error))
        exit(1)

def read_table_info(table_info_file_path):
    """
    Open and read contents of a table info file. This file should be in the following format:
    field_name data_type,
    field_name data_type

    args:
     @table_info_file_path - path to the table info file, by default located in /vpa/database/input/ dir
    """
    try:
        table_info = helpers.read_file(os.path.join(table_info_file_path))
    except FileNotFoundError:
        logging.error("{file_val} not found".format(file_val = table_info_file_path))

    return table_info

def create_table(table, freq, env, connection, table_structure):
    """
    Create a database table, the structure of the table is provided by table_structure file

    args:
     @table - table name that is prefixed with asx_, i.e. symbol, asx_ont_
     @freq - table's frequency identifier i.e. asx_ont_test_daily
     @env - table's environment identifier i.e. asx_ont_test
     @connection - database connection object
     @table_structure - table structure file
    """
    cursor = connection.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS {table_val}_{freq_val}_{env_val}(
    {table_structure_val}
    )""".format(
        table_val = table,
        freq_val = freq,
        env_val = env,
        table_structure_val = table_structure))
    connection.commit()
    cursor.close()

def drop_table(table, connection):
    """
    Drop a table from a database.

    args:
     @table - table name to drop
     @connection - database connection
    """

    cursor = connection.cursor()
    try:
        logging.info("Dropping {table_val}".format(table_val = table))
        cursor.execute(
                "DROP TABLE IF EXISTS {table_val}".format(table_val = table))
        connection.commit()
        logging.info("{table_val} dropped".format(table_val = table))
        cursor.close()
    except:
       logging.warning("Can not drop {table_val}, please check the spelling or database connection".format(
           table_val = table))
       cursor.close()

def write_to_table(table, connection, data, table_structure):
    """
    Write data to table.

    args:
     @table - table name
     @connection - database connection
     @data - data to write
     @table_structure - table structure information
    """
    number_of_columns = helpers.get_number_of_elements(data = table_structure, num_cols_to_omit = 1)
    columns_to_write = ",".join(["%s"] * number_of_columns)
    column_names = ", ".join(helpers.get_column_names(data = table_structure, num_cols_to_omit = 2))
    
    if not data:
        logging.warning("No data to write into {table_val}".format(table_val = table))
    else:
        try:
            logging.info("Writing {n} rows to {table_val}".format(n = len(data), table_val = table))
            cursor = connection.cursor()
            sql = """INSERT INTO {table_val} ({columns_val}) 
            VALUES
            ({number_of_columns_val})""".format(table_val = table, columns_val = column_names, number_of_columns_val = columns_to_write)
            cursor.executemany(sql, data)
            connection.commit()
            cursor.close()
        except mysql.connector.Error as error:
            logging.error("Could not write {data_val} to {table_val} {error_val}".format(
                data_val = data, table_val = table, error_val = error))
            cursor.close()

def get_all_tables(database, connection) -> List:
    """
    Select all tables from connection's database and return them.

    args:
     @database - database name
     @connection - connection object
    """

    result = [] # type: List
    try:
        cursor = connection.cursor()
        logging.info("Getting all tables from {database_val}".format(database_val = database))
        cursor.execute("SHOW TABLES IN {database_val}".format(database_val = database))
        for table in cursor:
            result.append(table[0])
        cursor.close()
    except:
        logging.error("Could not retrieve tables from {database_val}".format(database_val = database))
        cursor.close()

    return result

def select_from_table(table, connection) -> List:
    """
    Select all information from  table and return it

    args:
     @table - table name
     @connection - connection object
    """
    
    results = [] # type: List
    try:
        cursor = connection.cursor()
        logging.info("Getting all data from {table_val}".format(table_val = table))
        cursor.execute("SELECT * FROM {table_val}".format(table_val = table))
        for table_data in cursor:
            results.append(table_data)
        cursor.close()
        output = [list(row) for row in results]
    except:
        logging.error("Could not select any data from {table_val}".format(table_val = table))
        cursor.close()

    return output

def delete_from_table(table, connection, date_to_delete):
    """
    Delete entry in some table based on date key

    args:
      @table - table name
      @connection - database connection
      @date_to_delete - Date key value to delete entry for
    """
    try:
        logging.info("Deleting data for {date_val}  from {table_val}".format(date_val = date_to_delete, table_val = table))
        cursor = connection.cursor()
        sql = """DELETE FROM {table_val} WHERE date_au = '{date_val}'""".format(table_val = table, date_val = date_to_delete)
        cursor.execute(sql)
        connection.commit()
        cursor.close()
    except mysql.connector.Error as error:
        logging.error("Could not delete {date_val} from {table_val} {error_val}".format(date_val = date_to_delete, table_val = table, error_val = error))

def update_table(table, connection, symbol):
    """
    Update a table with some Symbol's data.

    args:
      @table - table name
      @connection - database connection
      @symbol - Symbol named tuple to update the table with
    """
    try:
        logging.info("Updating {date_val} for {table_val} with {data_val}".format(
            date_val = symbol.date_au,
            table_val = table,
            data_val = symbol))
        cursor = connection.cursor()
        sql = """
        UPDATE {table_val}
        SET
        name = '{name_val}',
        code = '{code_val}',
        industry = '{industry_val}',
        date_au = '{date_val}',
        open = {open_val},
        high = {high_val},
        low = {low_val},
        close = {close_val},
        volume = {volume_val},
        weekday_au = '{weekday_val}',
        date_utc_unixtimestamp = {date_utc_unixtimestamp_val},
        prev_open = {prev_open_val},
        prev_high = {prev_high_val},
        prev_low = {prev_low_val},
        prev_close = {prev_close_val},
        prev_volume = {prev_volume_val},
        update_unixtimestamp = {update_unixtimestamp_val}

        WHERE
        date_au = '{date_val}'
        """.format(
                table_val = table,
                name_val = symbol.name,
                code_val = symbol.code,
                industry_val = symbol.industry,
                date_val = symbol.date_au,
                open_val = symbol.open,
                high_val = symbol.high,
                low_val = symbol.low,
                close_val = symbol.close,
                volume_val = symbol.volume,
                weekday_val = symbol.weekday_au,
                date_utc_unixtimestamp_val = symbol.date_utc_unixtimestamp,
                prev_open_val = symbol.prev_open,
                prev_high_val = symbol.prev_high,
                prev_low_val = symbol.prev_low,
                prev_close_val = symbol.prev_close,
                prev_volume_val = symbol.prev_volume,
                update_unixtimestamp_val = symbol.update_unixtimestamp)
        cursor.execute(sql)
        connection.commit()
        cursor.close()
    except mysql.connector.Error as error:
        logging.error("Could not update {table_val} for {date_val} with {data_val} {error_val}".format(
            table_val = table,
            date_val = symbol.date_au,
            data_val = symbol,
            error_val = error))
 
