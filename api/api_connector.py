import requests
import logging
import csv
import mypy

from typing import List

logging.basicConfig(
        format = '%(asctime)s %(levelname)-1s %(message)s',
        level =  logging.WARN,
        datefmt = '%d-%m-%Y %H:%M:%S')

def get_alphavantage_url(type, symbol, api_key):
    """
    Build an AlphaVantage URL with specific time_series value for a symbol using required API key

    args:
      @type - time series type i.e. daily, intraday etc...
      @symbol - which ASX symbol to get the data for
      @api_key - provided api key

    """

    url = "https://www.alphavantage.co/query?function=TIME_SERIES_{type_val}&symbol={symbol_val}.AX&outputsize=compact&apikey={api_key_val}&datatype=csv".format(
            type_val = type,
            symbol_val = symbol,
            api_key_val = api_key)
    return url

def get_data_from_api(url) -> List:
    """
    Call some API to download and return csv data for a particular symbol

    args:
      @url - Some API  URL which contains time series data to return, symbol and api key
    """
    try:
        with requests.Session() as s:
            download = s.get(url, timeout = 10)
            decoded_content = download.content.decode('utf-8')
            output = csv.reader(decoded_content.splitlines(), delimiter=',')
            
            return list(output)
    except requests.exceptions.RequestException as e:
        logging.error("Error occured: {error_val}".format(error_val = e))
    except requests.exceptions.Timeout as f:
        logging.error("Timeout request for {url_val}: {f_val}".format(
            url_val = url,
            f_val = f))


def api_errors(output) -> bool:
    """
    Given API output check for any specific errors.

    args:
      @output - API call output
    """
    if "Error Message" in str(output):
        logging.error("API Call Error\n {output_val}".format(output_val = output))
        return True
    elif "standard API call frequency" in str(output) or "You exceeded your daily API requests limit" in str(output):
        logging.error("API Limit reached\n {output_val}".format(output_val = output))
        return True
    elif output == [['{}']] or len(output) <= 1 or not output:
        logging.error("No data returned by API, check your API key or consult API documentation/supporti, output: {output_val}".format(
            output_val = output))
        return True
    elif len(output[0]) not in [6,7]:
        logging.error("EOD API returned abnormal number of values, assuming an error in API return. output: {output_val}".format(
            output_val = output))
        return True
    else:
        return False

def get_eod_url(symbol, api_key) -> str:
    """
    Construct and return a https://eodhistoricaldata.com/ API url call.

    args:
      @symbol - ASX symbol to use, AX suffix will be added as default.
      @api_key - eodhistoricaldata.com API key
    """

    url = "https://eodhistoricaldata.com/api/eod/{symbol_val}.AU?api_token={api_key_val}&period=d&fmt=csv&from=2020-01-01".format(
            symbol_val = symbol,
            api_key_val = api_key.strip())

    return url

def remove_records(api_data, exclusion_values_set) -> List:
    """
    Given some API data list of lists remove empty entries

    args:
      @api_data - A list of lists representing Symbol data
      @xclusion_values_set - A set representation of values that shoud not be present in the API data
    """
    data = list(api_data)
    data_to_exclude = exclusion_values_set
    empty_records_removed  = [row for row in data if not data_to_exclude.intersection(row)]

    return empty_records_removed

def clean_eod_data(eod_data) -> List:
    """
     Given some eod csv file, do a cleaning pass on the data to remove un-needed fluff.

     args:
       @eod_data - Some EOD API csv output
    """
    data = list(eod_data)

    logging.info("Cleaning EOD API data")
    data.pop(len(data) - 1) # remove total values
    [row.pop(5) for row in data]

    return data

def get_workable_eod_data(eod_data, items_to_exclude) -> List:
    """
    Perform a clean and removal or records for an EOD API call

    args:
      @eod_data - Some EOD API csv output
      @items_to_exclude - A set of items that will determine if a record of an EOD data is kept or not
    """
    api_data = list(eod_data)
    cleaned_data = clean_eod_data(eod_data = api_data)
    data_without_invalid_records = remove_records(
            api_data = cleaned_data,
            exclusion_values_set = items_to_exclude)

    return data_without_invalid_records

def error_check_failed(error_check, symbol_code) -> bool:
    """
    If an error check is True then write out to log and skip current logic

    args:
      @error_check - True/False return of an error check
      @symbol_code - ASX Symbol code
    """
    if error_check is True:
        logging.info("Skipping processing for {company_code_val}".format(company_code_val = symbol_code))
        logging.info("-------------------------------------------------------")
        
        return True
