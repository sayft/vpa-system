import unittest
import logging
import mypy

from api import api_connector
logging.basicConfig(level=logging.WARNING)

class TestApi(unittest.TestCase):
    def test_clean_eod_data(self):
        input = [
                ['2020-01-01', '15.0', '17.1', '10.5', '10.6', '10.7', '100'],
                ['2020-01-02', '16.0', '18.1', '9.7', '9.8', '9.9', '101'],
                [2]]
        expected_output = [
                ['2020-01-01', '15.0', '17.1', '10.5', '10.6', '100'],
                ['2020-01-02', '16.0', '18.1', '9.7',  '9.8', '101']]
        actual_output = api_connector.clean_eod_data(eod_data = input)

        self.assertEqual(expected_output, actual_output)

    def test_remove_empty_records(self):
        input = [
                ['2020-01-01', '15.0', '17.1', '10.5', '10.6', '10.7', '100'],
                ['2020-01-02', '', '', '', '', '', '100'],
                ['2020-01-03', '', '17.1', '', '10.6', '10.7', '100'],
                [''],
                [""]]

        expected_output = [['2020-01-01', '15.0', '17.1', '10.5', '10.6', '10.7', '100']]
        actual_output = api_connector.remove_records(
                api_data = input, 
                exclusion_values_set = {'', ""})

        self.assertEqual(expected_output, actual_output)

    def test_remove_any_zero_records(self):
        input = [
                ['2020-01-01', '15.0', '17.1', '10.5', '10.6', '10.7', '100'],
                ['2020-01-02', '25.0', '11.3', '8.1', '8.5', '12.5', '0'],
                ['2020-01-03', 0, '17.1', '', '0', '10.7', '100'],
                ['0'],
                [0],
                ['2020-01-01', '0.1', '17.1', '10.5', '10.6', '10.7', '100'],
                ['2020-01-01', '15.0', 0.001, '10.5', '10.6', '10.7', '100'],
                ['01']]
        expected_output = [
                ['2020-01-01', '15.0', '17.1', '10.5', '10.6', '10.7', '100'],
                ['2020-01-01', '0.1', '17.1', '10.5', '10.6', '10.7', '100'],
                ['2020-01-01', '15.0', 0.001, '10.5', '10.6', '10.7', '100'],
                ['01']]
        actual_output = api_connector.remove_records(
                api_data = input,
                exclusion_values_set = {0, '0'})

        self.assertEqual(expected_output, actual_output)

